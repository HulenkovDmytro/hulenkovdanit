/*1) Обьяснить своими словами разницу между обьявлением переменных через var, let и const.
ключевое слово var служит для обьявления переменныл илии констант в одном лице, была в javascript изначально 
ключевое слово let с появлением "ECMAScript 6" используется для обьявления переменных
ключевое слово const с появлением "ECMAScript 6" используется для обьявления констант , тоесть значение запрещено к изменению.javascript

2) Почему объявлять переменную через var считается плохим тоном? 
До появления let и const var была во всей область видимости програмы в любом месте файла .js 
Область видимости переменных, объявленных с использованием var, оказывается слишком большой. Это может привести к непреднамеренной перезаписи данных и к другим ошибкам. .*/
'use strict'
let userName;
let userAge;
do {
    userName = prompt("What you name", userName);
    userAge = prompt("How old are you?", userAge);
} while
    (!userName || !(userAge && !isNaN(userAge) && typeof +userAge === "number"));

if (userAge < 18) {
    alert("You are not allowed to visit this website");
} else if (userAge > 18 && userAge <= 22) {
    // let result = confirm("Are you sure you want to continue?");
    if (confirm("Are you sure you want to continue?")) {
        alert(`"Welcome, ${userName}"`);
    } else {
        alert("You are not allowed to visit this website");
    }
} else {
    alert(`"Welcome, ${userName}"`);
}

