"use strict";

document.addEventListener('keydown', logKey);
function logKey(e) {
    const buttons = [...document.querySelectorAll(".btn")];

    buttons.forEach(el => {
        el.classList.remove("active");
        if (el.dataset.btn == e.code) {
            el.classList.add("active");
        }
    });
}
