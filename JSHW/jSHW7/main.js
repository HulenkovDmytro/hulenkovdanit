/* Опишите своими словами, как Вы понимаете, что такое Document Object Model DOM
Под Document Object Model подразумевают дерево DOM, поскольку она состоит из дерева объектов,
называемых узлами ониже node.Это дерево образуется за счет вложенной структуры тегов
плюс текстовые фрагменты страницы, каждый из которых образует отдельный узел.
*/
"use strict";

const newArray = [
  "hello",
  "world",
  "Kiev",
  "Kharkiv",
  "Odessa",
  "Lviv",
  "1",
  "2",
  "3",
  "sea",
  "user",
  23,
];

const renderList = (arr, dbody = document.body) => {
  let li = arr.map((e) => `<li>${e}</li>`);
  let clearLi = li.join("");
  let ul = document.createElement("ul");
  ul.innerHTML = clearLi;
  dbody.append(ul);
};

renderList(newArray);
