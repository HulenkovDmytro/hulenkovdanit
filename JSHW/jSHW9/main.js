"use strict";

let tabtitle = document.querySelectorAll(".tabs-title");
let content = document.querySelectorAll(".tab");
let target;

tabtitle.forEach(function (tabclick) {
  tabclick.addEventListener("click", (e) => {
    tabtitle.forEach((tabclick) => {
      tabclick.classList.remove("active");
    });
    tabclick.classList.add("active");
    target = tabclick.getAttribute("data-target");
    changecontent(target);
  });
});

function changecontent(target) {
  content.forEach((e) => {
    if (e.classList.contains(target)) {
      e.classList.add("active");
    } else e.classList.remove("active");
  });
}
